﻿#!/bin/sh

read -r -d '' day << EOF
using AoCHelper;

namespace AoC2022
{
    public partial class Day$1 : BaseDay
    {
        private readonly string _input;

        public Day$1()
        {
            _input = File.ReadAllText(InputFilePath);
        }

        public override ValueTask<string> Solve_1()
        {
            return ValueTask.FromResult("");
        }

        public override ValueTask<string> Solve_2()
        {
            return ValueTask.FromResult("");
        }
    }
}
EOF

echo "Provide the sample input and press ctrl-d when done:"
sample_input=$(cat)

echo "Provide the solution for the first sample and press ctrl-d when done:"
expected_sample=$(cat)

echo "Provide your puzzle input and press ctrl-d when done:"
puzzle_input=$(cat)

test_line="        public async Task SolveDay(Type type, string expectedPartOne, string expectedPartTwo)"

printf "%s\n" "$day" > "Aoc2022/Day$1.cs"
printf "%s" "$sample_input" > "TestInputs/Inputs/$1.txt"
printf "%s" "$puzzle_input" > "Aoc2022/Inputs/$1.txt"
sed -i 's/'"$test_line"'/        [TestCase(typeof(Day'"$1"'), "'"$expected_sample"'", "")]\n'"$test_line"'/g' TestInputs/Days.cs
