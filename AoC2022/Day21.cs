using AoCHelper;
using System.Collections.Generic;
using System.Collections.Immutable;

namespace AoC2022
{
    public partial class Day21 : BaseDay
    {
        private readonly string _input;

        public Day21()
        {
            _input = File.ReadAllText(InputFilePath);
        }

        public override ValueTask<string> Solve_1()
        {
            var monkeys = _input.Split(Environment.NewLine).Select(line => new Monkey(line)).ToImmutableDictionary(monkey => monkey.Name, monkey => monkey);
            return ValueTask.FromResult(monkeys["root"].GetResult(monkeys).ToString());
        }

        public override ValueTask<string> Solve_2()
        {
            var monkeys = _input.Split(Environment.NewLine).Select(line => new Monkey(line)).ToImmutableDictionary(monkey => monkey.Name, monkey => monkey);
            return ValueTask.FromResult(CalculateReverse(monkeys).ToString());
        }

        private long CalculateReverse(IDictionary<string, Monkey> monkeys)
        {
            var root = monkeys["root"];
            long expected;
            Monkey start;
            if (monkeys[root.Dependent.monkeyA].DependsOnHumn(monkeys))
            {
                expected = monkeys[root.Dependent.monkeyB].GetResult(monkeys);
                start = monkeys[root.Dependent.monkeyA];
            }
            else
            {
                expected = monkeys[root.Dependent.monkeyA].GetResult(monkeys);
                start = monkeys[root.Dependent.monkeyB];
            }
            return start.GetResultReverse(monkeys, expected);
        }

        private class Monkey
        {
            public string Name { get; }
            public (string monkeyA, string monkeyB) Dependent { get; }
            private Func<long, long, long> operation;
            private Func<long, long, long> operationReverse;
            private long? result = null;
            private char op;

            public Monkey(string line)
            {
                var split = line.Split(':');
                Name = split[0];
                split = split[1].Split(' ', StringSplitOptions.RemoveEmptyEntries);
                if (split.Length == 3)
                {
                    Dependent = (split[0], split[2]);
                    op = split[1][0];
                    operation = op switch
                    {
                        '*' => (long a, long b) => a * b,
                        '+' => (long a, long b) => a + b,
                        '-' => (long a, long b) => a - b,
                        '/' => (long a, long b) => a / b,
                        _ => throw new NotImplementedException()
                    };
                    operationReverse = op switch
                    {
                        '*' => (long a, long b) => a / b,
                        '+' => (long a, long b) => a - b,
                        '-' => (long a, long b) => a + b,
                        '/' => (long a, long b) => a * b,
                        _ => throw new NotImplementedException()
                    };
                } 
                else
                {
                    result = long.Parse(split[0]);
                    operation = (long a, long b) => result.Value;
                }
            }

            public bool DependsOnHumn(IDictionary<string, Monkey> monkeys)
            {
                return result.HasValue ? Name == "humn" : monkeys[Dependent.monkeyA].DependsOnHumn(monkeys) || monkeys[Dependent.monkeyB].DependsOnHumn(monkeys);
            }

            public long GetResult(IDictionary<string, Monkey> monkeys)
            {
                if (!result.HasValue)
                {
                    return operation.Invoke(monkeys[Dependent.monkeyA].GetResult(monkeys), monkeys[Dependent.monkeyB].GetResult(monkeys));
                }
                return result.Value;
            }
            public long GetResultReverse(IDictionary<string, Monkey> monkeys, long expected)
            {
                if (!result.HasValue)
                {
                    (var a, var b) = monkeys[Dependent.monkeyA].DependsOnHumn(monkeys) ?
                        (Dependent.monkeyB, Dependent.monkeyA) : 
                        (Dependent.monkeyA, Dependent.monkeyB);
                    // if original op is / or - the reversed op is dependent on position of param (e.g. 5 = 15 / 3 => 15 / 3 or 5 * 3)
                    var newExpected = a == Dependent.monkeyA && (op == '/' || op == '-') ? operation(monkeys[a].GetResult(monkeys), expected) :
                        operationReverse.Invoke(expected, monkeys[a].GetResult(monkeys));
                    return monkeys[b].GetResultReverse(monkeys, newExpected);
                }
                else if (Name == "humn")
                {
                    return expected;
                }
                else
                {
                    return result.Value;
                }
            }
        }
    }
}
