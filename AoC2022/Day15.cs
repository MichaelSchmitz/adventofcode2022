using AoCHelper;

namespace AoC2022
{
    public partial class Day15 : BaseDay
    {
        private readonly string _input;
        public static int Part1_Row = 2000000;
        public static int Part2_Space = 4000000;

        public Day15()
        {
            _input = File.ReadAllText(InputFilePath);
        }

        public override ValueTask<string> Solve_1()
        {
            var sensors = _input.Split(Environment.NewLine).Select(line => new Sensor(line));
            var beaconsOnRow = sensors.Where(sensor => sensor.Beacon.y == Part1_Row).Select(sensor => sensor.Beacon).ToHashSet().Count;
            var covered = sensors.Select(sensor => {
                if (sensor.GetCoveredForRow(Part1_Row, out var covered))
                    return covered;
                else 
                    return (int.MinValue, int.MaxValue);
                }).Where(covered => covered.Item1 != int.MinValue && covered.Item2 != int.MaxValue);
            var start = covered.MinBy(entry => entry.Item1).Item1;
            var end = covered.MaxBy(entry => entry.Item2).Item2;
            return ValueTask.FromResult((end - start + 1 - beaconsOnRow).ToString());
        }

        public override ValueTask<string> Solve_2()
        {
            var sensors = _input.Split(Environment.NewLine).Select(line => new Sensor(line));
            var (aCoefficents, bCoefficients) = sensors.Select(GetBoundingBox)
                .Aggregate((set, setTwo) => (set.a.Union(setTwo.a).ToHashSet(), set.b.Union(setTwo.b).ToHashSet()));
            foreach (var a in aCoefficents)
            {
                foreach (var b in bCoefficients)
                {
                    var coordinates = (x: (b - a) / 2, y: (a + b) / 2);
                    if (IsInRange(coordinates) && sensors.All(sensor => ManhattenDistance(sensor.Position, coordinates) > ManhattenDistance(sensor.Position, sensor.Beacon)))
                        return ValueTask.FromResult((coordinates.x * 4000000L + coordinates.y).ToString());
                }
            }

            return ValueTask.FromResult("Not found");
        }

        private bool IsInRange((int x, int y) coordinates)
        {
            return coordinates.x >= 0 && coordinates.x <= Part2_Space &&
                coordinates.y >= 0 && coordinates.y <= Part2_Space;
        }

        private (HashSet<int> a, HashSet<int> b) GetBoundingBox(Sensor sensor)
        {
            var radius = ManhattenDistance(sensor.Beacon, sensor.Position);
            var a = new HashSet<int>()
            {
                sensor.Position.y - sensor.Position.x + radius + 1,
                sensor.Position.y - sensor.Position.x - radius - 1,
            };
            var b = new HashSet<int>()
            {
                sensor.Position.y + sensor.Position.x + radius + 1,
                sensor.Position.y + sensor.Position.x - radius - 1,
            };
            return (a, b);
        }

        private class Sensor
        {
            public (int x, int y) Position { get; private set; }
            public (int x, int y) Beacon { get; private set; }
            public int Radius { get; private set; }
            
            public Sensor(string line) 
            {
                var split = line.Split(':');
                Position = ExtractCoordinates(split[0]);
                Beacon = ExtractCoordinates(split[1]);
                Radius = ManhattenDistance(Position, Beacon);
            }

            public bool GetCoveredForRow(int y, out (int start, int end) covered)
            {
                if (Position.y + Radius >= y && Position.y - Radius <= y)
                {
                    var count = Radius - Math.Abs(y - Position.y);
                    covered = (Position.x - count, Position.x + count);
                    return true;
                }
                covered = (0, 0);
                return false;
            }

            private (int x, int y) ExtractCoordinates(string part)
            {
                var split = part.Split("x=")[1].Split(", y=");
                return (int.Parse(split[0]), int.Parse(split[1]));
            }
        }

        private static int ManhattenDistance((int x, int y) a, (int x, int y) b)
        {
            return Math.Abs(a.x - b.x) + Math.Abs(a.y - b.y);
        }
    }
}
