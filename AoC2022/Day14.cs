using AoCHelper;
using System.Collections.Immutable;

namespace AoC2022
{
    public partial class Day14 : BaseDay
    {
        private readonly string _input;

        public Day14()
        {
            _input = File.ReadAllText(InputFilePath);
        }

        public override ValueTask<string> Solve_1()
        {
            var rocks = _input.Split(Environment.NewLine).Select(line => new Rock(line)).SelectMany(rock => rock.FilledPositions).ToImmutableHashSet();
            var sand = new HashSet<(int, int)>();
            var minX = rocks.MinBy(pos => pos.x).x;
            var maxX = rocks.MaxBy(pos => pos.x).x;
            var maxY = rocks.MaxBy(pos => pos.y).y;
            var notInAbyss = true;
            var counter = 0;
            while (notInAbyss)
            {
                notInAbyss = PerformSandFall(sand, rocks, minX, maxX, maxY, (x: 500, y: 0), out var _);
                counter++;
            }
            return ValueTask.FromResult((counter - 1).ToString()); // we count the abyss one as well in above loop
        }

        public override ValueTask<string> Solve_2()
        {
            var rocks = _input.Split(Environment.NewLine).Select(line => new Rock(line)).SelectMany(rock => rock.FilledPositions).ToList();
                       
            var sand = new HashSet<(int, int)>();
            var minX = rocks.MinBy(pos => pos.x).x - 500;
            var maxX = rocks.MaxBy(pos => pos.x).x + 500;
            var floor = rocks.MaxBy(pos => pos.y).y + 2;
            
            for (int x = minX; x <= maxX; x++)
            {
                rocks.Add((x, floor));
            }
            var setRocks = rocks.ToImmutableHashSet();
            var counter = 0;
            var initialSand = (500, 0);
            while (!sand.Contains((500, 0)))
            {
                _ = PerformSandFall(sand, setRocks, minX, maxX, floor, initialSand, out initialSand);
                counter++;
            }
            return ValueTask.FromResult((counter).ToString());
        }

        private bool PerformSandFall(HashSet<(int x, int y)> sand, ImmutableHashSet<(int x, int y)> rocks, int minX, int maxX, int maxY, (int x, int y) initialSand, out (int x, int y) nextSand)
        {
            var lastPos = initialSand;
            nextSand = (500, 0);
            var moved = true;
            while (moved)
            {
                if (!(rocks.Contains((lastPos.x, lastPos.y + 1)) || sand.Contains((lastPos.x, lastPos.y + 1))))
                {
                    nextSand = lastPos;
                    lastPos.y++;
                }
                else if (!(rocks.Contains((lastPos.x - 1, lastPos.y + 1)) || sand.Contains((lastPos.x - 1, lastPos.y + 1))))
                {
                    nextSand = lastPos;
                    lastPos.y++;
                    lastPos.x--;
                }
                else if (!(rocks.Contains((lastPos.x + 1, lastPos.y + 1)) || sand.Contains((lastPos.x + 1, lastPos.y + 1))))
                {
                    nextSand = lastPos;
                    lastPos.y++;
                    lastPos.x++;
                } else
                {
                    moved = false;
                }
                if (lastPos.y > maxY || lastPos.x < minX || lastPos.x > maxX)
                {
                    return false;
                } 
            }
            sand.Add(lastPos);
            return true;
        }


        private class Rock
        {
            public HashSet<(int x, int y)> FilledPositions { get; set; } = new();

            public Rock(string line)
            {
                var positions = line.Split(" -> ");
                var start = positions.ElementAt(0).Split(',').Select(num => int.Parse(num));
                var lastX = start.ElementAt(0);
                var lastY = start.ElementAt(1);                
                foreach (var position in positions.Skip(1))
                {
                    var nextPosition = position.Split(',').Select(num => int.Parse(num));
                    var nextX = nextPosition.ElementAt(0);
                    var nextY = nextPosition.ElementAt(1);
                    var xLoop = nextX > lastX ? (lastX, nextX) : (nextX, lastX);
                    var yLoop = nextY > lastY ? (lastY, nextY) : (nextY, lastY);
                    for (int y = yLoop.Item1; y <= yLoop.Item2; y++)
                    {
                        for (int x = xLoop.Item1; x <= xLoop.Item2; x++)
                        {
                            FilledPositions.Add((x, y));
                        }
                    }
                    lastX = nextX;
                    lastY = nextY;
                }
            }
        }
    }
}
