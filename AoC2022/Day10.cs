﻿using AoCHelper;
using System.Text;

namespace AoC2022
{
    public partial class Day10 : BaseDay
    {
        private readonly string _input;

        public Day10()
        {
            _input = File.ReadAllText(InputFilePath);

        }

        public override ValueTask<string> Solve_1()
        {
            var instructions = _input.Split(Environment.NewLine);
            return ValueTask.FromResult(GetSignalStrengths(instructions).ToString());
        }

        public override ValueTask<string> Solve_2()
        {
            var instructions = _input.Split(Environment.NewLine);
            return ValueTask.FromResult(string.Join(Environment.NewLine, DrawCRTImage(instructions)));
        }

        private int GetSignalStrengths(IEnumerable<string> instructions)
        {
            var cycleCount = 1;
            var signalStrengths = 0;
            var X = 1;
            foreach (var instruction in instructions)
            {
                if (instruction == "noop")
                {
                    cycleCount++;
                }
                else
                {
                    if ((cycleCount + 21) % 40 == 0)
                    {
                        signalStrengths += X * (cycleCount + 1);
                    }
                    cycleCount += 2;
                    X += int.Parse(instruction[5..]);
                }
                if ((cycleCount + 20) % 40 == 0)
                {
                    signalStrengths += X * cycleCount;
                }
                if (cycleCount >= 220)
                {
                    return signalStrengths;
                }
            }
            return signalStrengths;
        }

        private List<string> DrawCRTImage(IEnumerable<string> instructions)
        {
            var cycleCount = 0;
            var X = 1;
            var lines = new List<string>();
            var line = new StringBuilder();
            foreach (var instruction in instructions)
            {
                line.Append(GetNextCRTChar(cycleCount, X));
                if (instruction == "noop")
                {
                    cycleCount++;
                }
                else
                {
                    if ((cycleCount + 1) % 40 == 0)
                    {
                        lines.Add(line.ToString());
                        line = new StringBuilder();
                    }
                    line.Append(GetNextCRTChar(cycleCount + 1, X));
                    cycleCount += 2;
                    X += int.Parse(instruction[5..]);
                }

                if (cycleCount % 40 == 0)
                {
                    lines.Add(line.ToString());
                    line = new StringBuilder();
                }
                if (lines.Count == 6)
                    return lines;
            }
            return lines;
        }

        private char GetNextCRTChar(int cycleCount, int X)
        {
            var currentX = cycleCount % 40;
            return X >= currentX - 1 && X <= currentX + 1 ? '#' : '.';
        }
    }
}
