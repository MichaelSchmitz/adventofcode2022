﻿using AoCHelper;

namespace AoC2022
{
    public partial class Day07 : BaseDay
    {
        private readonly string _input;

        public Day07()
        {
            _input = File.ReadAllText(InputFilePath);

        }

        public override ValueTask<string> Solve_1()
        {
            var directories = ExtractFolderStructure(_input);
            return ValueTask.FromResult(directories.Select(d => d.Value.Size).Where(size => size <= 100000).Sum().ToString());
        }

        public override ValueTask<string> Solve_2()
        {
            var directories = ExtractFolderStructure(_input);
            var freeSpace = 70000000 - directories["/"].Size;
            var missingSpace = 30000000 - freeSpace;
            return ValueTask.FromResult(directories.Select(d => d.Value.Size).Where(size => size >= missingSpace).Order().First().ToString());
        }

        private Dictionary<string, Directory> ExtractFolderStructure(string input)
        {
            var result = new Dictionary<string, Directory>();
            IEnumerable<string> lines = input.Split(Environment.NewLine);
            var currentWorkingDirectory = lines.First()[5..];
            lines = lines.Skip(1).Where(l => l != "$ ls");
            result.Add("/", new());
            foreach (var line in lines)
            {
                if (line.StartsWith("$ cd"))
                {
                    var pwd = currentWorkingDirectory == "/" ? "" : currentWorkingDirectory;                    
                    if (line == "$ cd ..")
                    {
                        currentWorkingDirectory = currentWorkingDirectory[..currentWorkingDirectory.LastIndexOf('/')];
                    }
                    else
                    {
                        currentWorkingDirectory = $"{pwd}/{line[5..]}";
                    }
                } else if (line.StartsWith("dir"))
                {
                    var dir = new Directory();
                    var pwd = currentWorkingDirectory == "/" ? "" : currentWorkingDirectory;
                    result[currentWorkingDirectory].Add(dir);
                    result.Add($"{pwd}/{line[4..]}", dir);
                } 
                else
                {
                    var file = new PuzzleFile(line);
                    result[currentWorkingDirectory].Add(file);
                }
            }
            return result;
        }

        private class Directory
        {
            public List<PuzzleFile> Files { get; private set; } = new();
            public List<Directory> Subdirectories { get; private set; } = new();
            public int Size { get => Files.Select(f => f.Size).Sum() + Subdirectories.Select(d => d.Size).Sum(); }

            public void Add(Directory dir)
            {
                Subdirectories.Add(dir);
            }

            public void Add(PuzzleFile file)
            {
                Files.Add(file);
            }
        }

        private class PuzzleFile
        {
            public string Name { get; private set; }
            public int Size { get; private set; }

            public PuzzleFile(string line)
            {
                var split = line.Split(' ');
                Size = int.Parse(split[0]);
                Name = split[1];
            }
        }

    }
}
