using AoCHelper;
using System.Security.Cryptography.X509Certificates;

namespace AoC2022
{
    public partial class Day23 : BaseDay
    {
        private readonly string _input;

        public Day23()
        {
            _input = File.ReadAllText(InputFilePath);
        }

        public override ValueTask<string> Solve_1()
        {
            var elves = _input.Split(Environment.NewLine).SelectMany((line, y) => line.Select((c, x) =>
            {
                if (c == '.') return null;
                else return new Position(x, y);
            })).Where(entry => entry != null).ToHashSet();
            for (int i = 0; i < 10; i++)
            {
                ComputeRound(elves, i);
            }
            return ValueTask.FromResult(CalculateEmptyTiles(elves).ToString());
        }

        public override ValueTask<string> Solve_2()
        {
            var elves = _input.Split(Environment.NewLine).SelectMany((line, y) => line.Select((c, x) =>
            {
                if (c == '.') return null;
                else return new Position(x, y);
            })).Where(entry => entry != null).ToHashSet();
            var i = 0;
            while (ComputeRound(elves, i))
            {
                i++;
            }
            return ValueTask.FromResult((i + 1).ToString());
        }

        private bool ComputeRound(HashSet<Position> elves, int directionOffset)
        {
            var proposedTargets = new Dictionary<Position, Position>();
            foreach (var elf in elves)
            {
                for (int i = 0; i < 4; i++)
                {
                    (var newDirection, var neighbours) = GetNeighbours(i, directionOffset);
                    if (AllNeighbours.All(neighbour => !elves.Contains(Add(elf, neighbour))))
                    {
                        break;
                    }
                    else if (!neighbours.Any(neighbour => elves.Contains(Add(elf, neighbour))))
                    {
                        proposedTargets.Add(elf, Add(elf, newDirection));
                        break;
                    }
                }                
            }
            var anyMoved = false;
            // do nothing for elves that want to move to the same position
            foreach (var target in proposedTargets)
            {
                if (proposedTargets.Count(t => t.Value == target.Value) == 1)
                {
                    anyMoved = true;
                    elves.Remove(target.Key);
                    elves.Add(target.Value);
                }
            }
            return anyMoved;
        }

        private record class Position (int x, int y);

        private (Position newDirection, List<Position> neighbours) GetNeighbours(int i, int offset)
        {
            var index = (i + offset) % 4;
            return index switch
            {
                0 => (new(0, -1), NorthNeighbours),
                1 => (new(0, 1), SouthNeighbours),
                2 => (new(-1, 0), WestNeighbours),
                3 => (new(1, 0), EastNeighbours),
            };
        }

        private Position Add(Position elf, Position offset)
        {
            return new(elf.x + offset.x, elf.y + offset.y);
        }


        private List<Position> AllNeighbours = new()
        {
            new(0, -1),
            new(0, 1),
            new(1, 0),
            new(1, 1),
            new(1, -1),
            new(-1, 0),
            new(-1, 1),
            new(-1, -1)
        };
        private List<Position> NorthNeighbours = new()
        {
            new(0, -1),
            new(1, -1),
            new(-1, -1)
        };
        private List<Position> EastNeighbours = new()
        {
            new(1, 0),
            new(1, 1),
            new(1, -1)
        };
        private List<Position> SouthNeighbours = new()
        {
            new(0, 1),
            new(1, 1),
            new(-1, 1)
        };
        private List<Position> WestNeighbours = new()
        {
            new(-1, 0),
            new(-1, 1),
            new(-1, -1)
        };

        private int CalculateEmptyTiles(IEnumerable<Position> elves)
        {
            var xMin = elves.MinBy(elf => elf.x).x;
            var xMax = elves.MaxBy(elf => elf.x).x;
            var yMin = elves.MinBy(elf => elf.y).y;
            var yMax = elves.MaxBy(elf => elf.y).y;
            var x = xMax + (xMin < 0 ? -xMin : xMin) + 1;
            var y = yMax + (yMin < 0 ? -yMin : yMin) + 1;
            return (x * y) - elves.Count();
        }
    }
}
