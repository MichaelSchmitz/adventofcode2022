using AoCHelper;
using System.Collections.Immutable;

namespace AoC2022
{
    public partial class Day12 : BaseDay
    {
        private readonly string _input;

        public Day12()
        {
            _input = File.ReadAllText(InputFilePath);
        }

        public override ValueTask<string> Solve_1()
        {
            var (map, start, end) = GetMap(_input);
            return ValueTask.FromResult(FindShortestPath(map, new() { start }, end).ToString());
        }

        public override ValueTask<string> Solve_2()
        {
            var (map, _, end) = GetMap(_input);
            var startingPoints = map.Where(entry => entry.Value.Height == 'a').Select(entry => entry.Key).ToList();
            return ValueTask.FromResult(FindShortestPath(map, startingPoints, end).ToString());
        }

        private (IDictionary<(int x, int y), Point>, (int x, int y), (int x, int y)) GetMap(string input)
        {
            IDictionary<(int x, int y), Point> map = new Dictionary<(int x, int y), Point>();
            var lines = input.Split(Environment.NewLine);
            var start = (-1, -1);
            var end = (-1, -1);
            for (int y = 0; y < lines.Length; y++)
            {
                var line = lines.ElementAt(y);
                for (int x = 0; x < line.Length; x++)
                {
                    char c = line.ElementAt(x);
                    if (c == 'S')
                        start = (x, y);
                    else if (c == 'E')
                        end = (x, y);
                    map.Add((x, y), new Point(c));
                }
            }
            map = map.ToImmutableDictionary(); 
            foreach (var point in map)
            {
                foreach (var neighbourModifier in NeighbourModifiers)
                {
                    var newPosition = GetPosition(point.Key, neighbourModifier);
                    if (!IsOutOfBounds(newPosition, lines[0].Length - 1, lines.Length - 1) && CanMoveTo(point.Value.Height, map[newPosition].Height)) {
                        point.Value.AllowedNeighbours.Add(newPosition);
                    }
                }
            }
            foreach (var point in map)
            {
                point.Value.Distance = CalculateDistance(point.Key, end);
            }
            return (map, start, end);
        }

        private int FindShortestPath(IDictionary<(int x, int y), Point> map, List<(int x, int y)> startingPoints, (int x, int y) end)
        {
            var visited = new HashSet<(int x, int y)>();
            var active = startingPoints.Select(start => (start, 0)).ToDictionary(start => start.start, start => start.Item2);
            while (active.Any())
            {
                var nextPoint = active.MinBy(point => map[point.Key].Distance + point.Value);
                if (AreEqual(end, nextPoint.Key))
                {
                    return nextPoint.Value;
                }
                visited.Add(nextPoint.Key);
                active.Remove(nextPoint.Key);

                var neighbours = map[nextPoint.Key].AllowedNeighbours;
                foreach (var neighbour in neighbours)
                {
                    if (visited.Contains(neighbour))
                        continue;
                    if (active.TryGetValue(neighbour, out int previous))
                    {
                        if (previous > nextPoint.Value + 1)
                        {
                            active[neighbour] = nextPoint.Value + 1;
                        }
                    }
                    else
                    {
                        active.Add(neighbour, nextPoint.Value + 1);
                    }
                }
            }
            return int.MaxValue;
        }

        private (int x, int y) GetPosition((int x, int y) point, (int x, int y) modifier) => (point.x + modifier.x, point.y + modifier.y);
        private bool IsOutOfBounds((int x, int y) point, int maxX, int maxY) => point.x < 0 || point.x > maxX || point.y < 0 || point.y > maxY;
        private bool CanMoveTo(char currentHeight, char nextHeight) => nextHeight - currentHeight <= 1;
        private bool AreEqual((int x, int y) a, (int x, int y) b) => a.x == b.x && a.y == b.y;
        private int CalculateDistance((int x, int y) self, (int x, int y) target) => Math.Abs(target.x - self.x) + Math.Abs(target.y - self.y);

        private List<(int x, int y)> NeighbourModifiers = new()
        {
            (0, 1),
            (0, -1),
            (1, 0),
            (-1, 0)
        };

        private class Point
        {
            public char Height { get; }
            public int Distance { get; set; }
            public List<(int x, int y)> AllowedNeighbours { get; } = new();

            public Point(char height)
            {
                Height = height == 'S' ? 'a' : height == 'E' ? 'z' : height;
            }

        }
    }
}
