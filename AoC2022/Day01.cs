﻿using AoCHelper;

namespace AoC2022
{
    public class Day01 : BaseDay
    {
        private readonly string _input;

        public Day01()
        {
            _input = File.ReadAllText(InputFilePath);
        }

        public override ValueTask<string> Solve_1()
        {
            var elves = _input.Split($"{Environment.NewLine}{Environment.NewLine}").Select(calories => new Elf(calories));
            return ValueTask.FromResult(elves.Max(elf => elf.Calories.Sum()).ToString());
        }

        public override ValueTask<string> Solve_2()
        {
            var elves = _input.Split($"{Environment.NewLine}{Environment.NewLine}").Select(calories => new Elf(calories));
            return ValueTask.FromResult(elves.OrderBy(elf => elf.Calories.Sum()).TakeLast(3).Select(elf => elf.Calories.Sum()).Sum().ToString());
        }

        private class Elf
        {
            public List<int> Calories { get; private set; }

            public Elf(string input) {
                var calories = input.Split(Environment.NewLine);
                Calories = calories.Select(calory => int.Parse(calory)).ToList();
            }
        }
    }
}
