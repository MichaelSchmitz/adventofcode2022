using AoCHelper;
using System.Collections.Immutable;
using System.Numerics;

namespace AoC2022
{
    public partial class Day11 : BaseDay
    {
        private readonly string _input;

        public Day11()
        {
            _input = File.ReadAllText(InputFilePath);

        }

        public override ValueTask<string> Solve_1()
        {
            var monkeys = _input.Split($"{Environment.NewLine}{Environment.NewLine}").Select(monkey => new Monkey(monkey)).ToImmutableArray();
            
            for (int i = 0; i < 20; i++)
            {
                foreach(var monkey in monkeys)
                {
                    MonkeyTurn(monkey, monkeys, worryLevel => worryLevel / 3);
                }
            }
            return ValueTask.FromResult(monkeys.Select(monkey => monkey.PerformedActions).OrderDescending().Take(2).Aggregate((m1, m2) => m1 * m2).ToString());
        }

        public override ValueTask<string> Solve_2()
        {
            var monkeys = _input.ReplaceLineEndings().Split($"{Environment.NewLine}{Environment.NewLine}").Select(monkey => new Monkey(monkey)).ToImmutableArray();

            var commonModulo = monkeys.Aggregate(1, (modulo, monkey) => modulo * monkey.Modulo);
            for (int i = 0; i < 10000; i++)
            {
                foreach (var monkey in monkeys)
                {
                    MonkeyTurn(monkey, monkeys, worryLevel => worryLevel % commonModulo);
                }
            }
            return ValueTask.FromResult(monkeys.Select(monkey => monkey.PerformedActions).OrderDescending().Take(2).Aggregate((m1, m2) => m1 * m2).ToString());
        }

        private void MonkeyTurn(Monkey monkey, ImmutableArray<Monkey> monkeys, Func<long, long> DecreaseWorryLevel)
        {
            foreach (var item in monkey.WorryLevelItems)
            {
                var worryLevel = monkey.ChangeWorryLevel(item);
                worryLevel = DecreaseWorryLevel(worryLevel);
                monkeys.ElementAt(monkey.GetMonkeyForThrow(worryLevel)).WorryLevelItems.Add(worryLevel);
            }
            monkey.WorryLevelItems.Clear();
        }

        private class Monkey
        {
            public List<long> WorryLevelItems { get; }
            public Func<long, long> ChangeWorryLevel { get; }
            public Func<long, int> GetMonkeyForThrow { get; }
            public long PerformedActions { get; private set; } = 0;
            public int Modulo { get; }
            private Func<long, long, long> PerformOperation { get; }

            public Monkey(string monkey)
            {
                var lines = monkey.Split(Environment.NewLine).Skip(1);
                WorryLevelItems = lines.ElementAt(0)[18..].Split(", ").Select(item => long.Parse(item)).ToList();
                var op = lines.ElementAt(1)[23];
                if (op == '+')
                {
                    PerformOperation = (old, old2) => old + old2;
                } else
                {
                    PerformOperation = (old, old2) => old * old2;
                }
                if (lines.ElementAt(1).EndsWith("old"))
                {
                    ChangeWorryLevel = (old) => PerformOperation(old, old);
                }
                else
                {
                    ChangeWorryLevel = (old) => PerformOperation(old, long.Parse(lines.ElementAt(1)[24..]));
                }
                Modulo = int.Parse(lines.ElementAt(2)[21..]);
                var monkeyTrue = int.Parse(lines.ElementAt(3)[28..]);
                var monkeyFalse = int.Parse(lines.ElementAt(4)[29..]);
                GetMonkeyForThrow = (val) =>
                {
                    PerformedActions++;
                    return val % Modulo == 0 ? monkeyTrue : monkeyFalse;
                };

            }
        }
    }
}
