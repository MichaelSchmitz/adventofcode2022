﻿using AoCHelper;
using System.Collections.Immutable;

namespace AoC2022
{
    public partial class Day17 : BaseDay
    {
        private readonly string _input;

        public Day17()
        {
            _input = File.ReadAllText(InputFilePath);
        }

        public override ValueTask<string> Solve_1()
        {
            var instructions = LoopItems(_input.Select(c => c == '<' ? -1 : 1)).GetEnumerator();
            return ValueTask.FromResult(CalculateRockHeight(instructions, 2022).ToString());
        }

        public override ValueTask<string> Solve_2()
        {
            var instructions = LoopItems(_input.Select(c => c == '<' ? -1 : 1)).GetEnumerator();
            return ValueTask.FromResult(CalculateRockHeight(instructions, 1000000000000).ToString());
        }

        private long Height(long tower, long removed)
        {
            return tower + removed - 1;
        }

        private long CalculateRockHeight(IEnumerator<int> instructions, long numberOfRocks)
        {
            var tower = new List<string>() { "+-------+" };
            
            var rocks = LoopItems(new List<Rock>()
            {
                new Rock("####"),
                new Rock(" # ", "###", " # "),
                new Rock("  #", "  #", "###"),
                new Rock("#", "#", "#", "#"),
                new Rock("##", "##")
            }).GetEnumerator();

            var seen = new Dictionary<string, (long rockCount, long height)>();
            var removed = 0L;
            while (numberOfRocks > 0)
            {
                if (seen.TryGetValue(ToKey(tower), out var cached))
                {
                    var periodLength = cached.rockCount - numberOfRocks;
                    var height = Height(tower.Count, removed) - cached.height;
                    removed += (numberOfRocks / periodLength) * height;
                    numberOfRocks %= periodLength;
                    break;
                }
                else {
                    rocks.MoveNext();
                    seen.Add(ToKey(tower), (numberOfRocks, Height(tower.Count, removed)));
                    removed += MoveRock(rocks.Current, tower, instructions);
                    numberOfRocks--;
                }
            }
            while (numberOfRocks > 0)
            {
                numberOfRocks--;
                rocks.MoveNext();
                removed += MoveRock(rocks.Current, tower, instructions);
            }
            return Height(tower.Count, removed);            
        }

        private string ToKey(List<string> tower)
        {
            return string.Join("\n", tower);
        }

        private int MoveRock(Rock rock, List<string> tower, IEnumerator<int> instructions)
        {
            var stopped = false;
            for (int i = 0; i < rock.Height + 3; i++)
            {
                tower.Insert(0, "|       |");
            }
            var position = (x: 3, y: 0);
            while (!stopped)
            {
                instructions.MoveNext();
                if (!HitSomething(tower, rock, position, xModifier: instructions.Current))
                {
                    position.x += instructions.Current;
                }
                if (HitSomething(tower, rock, position, yModifier: 1))
                {
                    stopped = true;
                }
                else
                {
                    position.y++;
                }
            }
            return InsertRock(tower, rock, position);
        }

        private bool HitSomething(List<string> tower, Rock rock, (int x, int y) rockPosition, int xModifier = 0, int yModifier = 0)
        {
            for (int y = 0; y < rock.Height; y++)
            {
                for (int x = 0; x < rock.Width; x++)
                {
                    var xPosition = x + rockPosition.x + xModifier;
                    var yPosition = y + rockPosition.y + yModifier;
                    if (rock.Shape[y][x] == '#' & tower[yPosition][xPosition] != ' ')
                    {
                        return true;
                    }
                }
            }
            return false;
        }

        private int InsertRock(List<string> tower, Rock rock, (int x, int y) rockPosition)
        {
            for (int y = 0; y < rock.Height; y++)
            {
                var line = tower[y + rockPosition.y].ToArray();
                for (int x = 0; x < rock.Width; x++)
                {
                    if (rock.Shape[y][x] == '#')
                    {
                        line[x + rockPosition.x] = '#';
                    }
                }
                tower[y + rockPosition.y] = string.Join("", line);
            }
            while (!tower.First().Contains('#'))
            {
                tower.RemoveAt(0);
            }
            if (tower.Count > 100)
            {
                var removeCount = tower.Count - 101;
                tower.RemoveRange(100, removeCount);
                return removeCount;
            }
            return 0;
        }

        private class Rock
        {
            public ImmutableArray<string> Shape { get; }
            public int Height { get; }
            public int Width { get; }

            public Rock(params string[] rockShape)
            {
                Shape = rockShape.ToImmutableArray();
                Height = rockShape.Length;
                Width = rockShape[0].Length;
            }
        }

        IEnumerable<T> LoopItems<T>(IEnumerable<T> items)
        {
            while (true)
            {
                foreach (var item in items)
                {
                    yield return item;
                }
            }
        }
    }
}
