using AoCHelper;
using System.Collections.Immutable;

namespace AoC2022
{
    public partial class Day24 : BaseDay
    {
        private readonly string _input;
        private static int basinSizeX;
        private static int basinSizeY;

        public Day24()
        {
            _input = File.ReadAllText(InputFilePath);
        }

        public override ValueTask<string> Solve_1()
        {
            var split = _input.Split(Environment.NewLine);
            basinSizeY = split.Length;
            basinSizeX = split.First().Length;
            (var start, var end, var blizzards) = FindBlizzardPattern(_input);
            return ValueTask.FromResult(FindRoute(start, end, blizzards).time.ToString());
        }

        public override ValueTask<string> Solve_2()
        {
            var split = _input.Split(Environment.NewLine);
            basinSizeY = split.Length;
            basinSizeX = split.First().Length;
            (var start, var end, var blizzards) = FindBlizzardPattern(_input);
            var state = FindRoute(start, end, blizzards);
            state = FindRoute(state, start.position, blizzards);
            state = FindRoute(state, end, blizzards);
            return ValueTask.FromResult(state.time.ToString());
        }

        private (State start, Position end, IDictionary<int, string[]> blizzards) FindBlizzardPattern(string input)
        {
            var result = new List<string[]>();
            var blizzards = ParseBlizzards(input);
            var blizzardMap = GenerateMap(input, blizzards);
            while (true)
            {
                result.Add(blizzardMap);
                blizzards = blizzards.Select(blizzard => blizzard.Advance()).ToList();
                blizzardMap = GenerateMap(input, blizzards);
                if (string.Join(' ', blizzardMap) == string.Join(' ', result[0])) break;
            }

            return (new State(0, new Position(1, 0)), new Position(basinSizeX - 2, basinSizeY - 1), result.Select((entry, index) => (index, entry)).ToImmutableDictionary(entry => entry.index, entry => entry.entry));

        }

        private List<Blizzard> ParseBlizzards(string input)
        {
            var lines = input.Split(Environment.NewLine);
            var result = new List<Blizzard>();
            for (var y = 0; y < basinSizeY; y++)
            {
                for (var x = 0; x < basinSizeX; x++)
                {
                    var c = lines[y][x];
                    if (c != '.' && c != '#')
                    {
                        result.Add(new Blizzard(x, y, c));
                    }
                }
            }
            return result;
        }

        private string[] GenerateMap(string input, List<Blizzard> blizzards)
        {
            var map = input.Replace('<', '.').Replace('>', '.').Replace('^', '.').Replace('v', '.').Split(Environment.NewLine).Select(line => line.ToCharArray()).ToArray();

            foreach (var blizzard in blizzards)
            {
                map[blizzard.Position.y][blizzard.Position.x] = 'B';
            }
            return map.Select(line => string.Join("", line)).ToArray();
        }

        private State FindRoute(State initial, Position end, IDictionary<int, string[]> blizzards)
        {
            var queue = new PriorityQueue<State, int>();

            queue.Enqueue(initial, Distance(end, initial.position) + initial.time);

            var seen = new HashSet<State>();
            while (queue.TryDequeue(out var state, out var time))
            {
                if (state.position == end) return state;
                
                foreach (var neighbour in GetNeighbours(state, blizzards))
                {
                    if (!seen.Contains(neighbour))
                    {
                        seen.Add(neighbour);
                        queue.Enqueue(neighbour, Distance(end, neighbour.position) + neighbour.time);
                    }
                }
            }
            return null;
        }

        private IEnumerable<State> GetNeighbours(State state, IDictionary<int, string[]> blizzards)
        {
            foreach (var newPosition in new Position[]
            {
                state.position,
                state.position with {x = state.position.x - 1},
                state.position with {x = state.position.x + 1},
                state.position with {y = state.position.y - 1},
                state.position with {y = state.position.y + 1},
            })
            {
                if (GetFieldForTime(blizzards, state.time + 1, newPosition) == '.')
                {
                    yield return state with
                    {
                        time = state.time + 1,
                        position = newPosition
                    };
                }
            }
        }

        private char GetFieldForTime(IDictionary<int, string[]> blizzards, int time, Position position)
        {
            var blizzardsAtTime = blizzards[time % blizzards.Count];

            return (position.x < 0 || position.y < 0 || position.x >= basinSizeX || position.y >= basinSizeY) ? '#' : blizzardsAtTime[position.y][position.x];
        }

        private int Distance(Position a, Position b)
        {
            return Math.Abs(a.x - b.x) + Math.Abs(a.y - b.y);
        }

        private record class State(int time, Position position);

        private record class Position (int x, int y);

        private record class Blizzard
        {
            public Position Position { get; private set; }
            private readonly Position direction;

            public Blizzard(int x, int y, char direction)
            {
                Position = new(x, y);
                this.direction = direction switch
                {
                    '>' => new Position(1, 0),
                    '<' => new Position(-1, 0),
                    '^' => new Position(0, -1),
                    'v' => new Position(0, 1)
                };
            }

            public Blizzard Advance()
            {
                var x = direction.x + Position.x;
                var y = direction.y + Position.y;
                if (x == 0)
                {
                    x = basinSizeX - 2;
                } else if (x == basinSizeX - 1)
                {
                    x = 1;
                }
                if (y == 0)
                {
                    y = basinSizeY - 2;
                }
                else if (y == basinSizeY - 1)
                {
                    y = 1;
                }
                return this with { Position = new Position(x, y) };
            }
        }
    }
}
