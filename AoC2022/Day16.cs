using AoCHelper;
using System.Collections.Immutable;

namespace AoC2022
{
    public partial class Day16 : BaseDay
    {
        private readonly string _input;

        public Day16()
        {
            _input = File.ReadAllText(InputFilePath);
        }

        public override ValueTask<string> Solve_1()
        {
            var valves = _input.Split(Environment.NewLine).Select(line => new Valve(line)).ToImmutableDictionary(valve => valve.Identifier, valve => valve);
            var (distances, positiveFlowValves) = FindDistances(valves);
            positiveFlowValves.RemoveWhere(valve => valve.Identifier == "AA");
            return ValueTask.FromResult(CalculateBestRoute(distances.ToImmutableDictionary(), positiveFlowValves, valves["AA"]).ToString());
        }

        public override ValueTask<string> Solve_2()
        {
            var valves = _input.Split(Environment.NewLine).Select(line => new Valve(line)).ToImmutableDictionary(valve => valve.Identifier, valve => valve);
            var (distances, positiveFlowValves) = FindDistances(valves);
            positiveFlowValves.RemoveWhere(valve => valve.Identifier == "AA");
            return ValueTask.FromResult(CalculateBestRouteForTwo(distances.ToImmutableDictionary(), positiveFlowValves, valves["AA"]).ToString());
        }

        private int CalculateBestRoute(IDictionary<(string start, string target), int> distances, HashSet<Valve> remainingValves, Valve currentValve, 
            int minutesRemaining = 30, Dictionary<string, int> cache = default)
        {
            string key = $"{minutesRemaining}_{currentValve.Identifier}_{string.Join("_", remainingValves.OrderBy(valve => valve.Identifier).Select(valve => valve.Identifier))}";
            if (cache == default) cache = new();
           
            if (!cache.ContainsKey(key))
            {
                var flowForCurrent = currentValve.FlowRate * minutesRemaining;

                var flowForOthers = 0;
                foreach (var valve in remainingValves.ToArray())
                {
                    var distance = distances[(currentValve.Identifier, valve.Identifier)];

                    if (minutesRemaining >= distance + 1)
                    {
                        remainingValves.Remove(valve);
                        flowForOthers = Math.Max(flowForOthers, CalculateBestRoute(distances, remainingValves, valve, minutesRemaining - distance - 1, cache));
                        remainingValves.Add(valve);
                    }
                }
                cache[key] = flowForCurrent + flowForOthers;
            }
            return cache[key];
        }

        private int CalculateBestRouteForTwo(IDictionary<(string start, string target), int> distances, HashSet<Valve> remainingValves, Valve start)
        {
            var pairings = GetAllPairings(remainingValves);
            return pairings.Select(pairing => 
                CalculateBestRoute(distances, pairing.human, start, minutesRemaining: 26) + 
                CalculateBestRoute(distances, pairing.elephant, start, minutesRemaining: 26)).Max();
        }

        private IEnumerable<(HashSet<Valve> human, HashSet<Valve> elephant)> GetAllPairings(HashSet<Valve> valves)
        {
            var maxPairing = 1 << (valves.Count - 1);

            for (var pairingOffset = 0; pairingOffset < maxPairing; pairingOffset++)
            {
                var elephant = new HashSet<Valve>();
                var human = new HashSet<Valve>();

                elephant.Add(valves.ElementAt(0));

                for (var i = 1; i < valves.Count; i++)
                {
                    if ((pairingOffset & (1 << i)) == 0)
                        human.Add(valves.ElementAt(i));
                    else
                        elephant.Add(valves.ElementAt(i));
                }
                yield return (human, elephant);
            }
        }

        private (Dictionary<(string start, string target), int>, HashSet<Valve>) FindDistances(IDictionary<string, Valve> valves)
        {
            var distances = new Dictionary<(string start, string target), int>();
            var valvesWithPositiveFlow = valves.Where(valve => valve.Value.FlowRate > 0 || valve.Key == "AA").Select(entry => entry.Value).ToHashSet();
            foreach (var startingValve in valvesWithPositiveFlow)
            {
                var nextValves = new HashSet<string> { startingValve.Identifier };
                var distance = 0;
                distances.Add((startingValve.Identifier, startingValve.Identifier), 0);
                while (nextValves.Any())
                {
                    distance++;
                    var newValves = new HashSet<string>();
                    foreach (var nextValve in nextValves)
                    {
                        var neighbours = valves[nextValve].Valves;
                        foreach (var neighbour in neighbours)
                        {
                            if (!distances.ContainsKey((startingValve.Identifier, neighbour)))
                            {
                                distances.Add((startingValve.Identifier, neighbour), distance);
                                newValves.Add(neighbour);
                            }
                        }
                    }
                    nextValves = newValves;
                }
            }
            
            return (distances, valvesWithPositiveFlow);
        }

        private class Valve
        {
            public string Identifier { get; private set; }
            public List<string> Valves { get; private set; } = new();
            public int FlowRate { get; private set; }

            public Valve(string line)
            {
                var split = line.Split(';');
                Identifier = split[0][6..8];
                FlowRate= int.Parse(split[0].Split('=')[1]);
                var valves = split[1].Split(", ");
                Valves.Add(valves[0][^2..]);
                foreach (var valve in valves.Skip(1))
                {
                    Valves.Add(valve);
                }
            }
        }
    }
}
