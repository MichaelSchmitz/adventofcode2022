﻿using AoCHelper;

namespace AoC2022
{
    public class Day02 : BaseDay
    {
        private readonly string _input;

        public Day02()
        {
            _input = File.ReadAllText(InputFilePath);
        }

        public override ValueTask<string> Solve_1()
        {
            var objects = _input.Split(Environment.NewLine).Select(o =>
            {
                var os = o.Split(' ');
                return (GetElfObject(os[0][0]), GetSelfObject(os[1][0]));
            });
            return ValueTask.FromResult(objects.Select(os => (int) os.Item2 + CalculateWin(os.Item1, os.Item2)).Sum().ToString());
        }

        public override ValueTask<string> Solve_2()
        {
            var objects = _input.Split(Environment.NewLine).Select(o =>
            {
                var os = o.Split(' ');
                return (GetElfObject(os[0][0]), GetSelfObjectPartTwo(os[1][0]));
            });
            return ValueTask.FromResult(objects.Select(os => os.Item2 + CalculateWin(os.Item1, os.Item2)).Sum().ToString());
        }

        private enum SelectedObject
        {
            Rock = 1,
            Paper = 2,
            Scissors = 3
        }

        private SelectedObject GetElfObject(char input)
        {
            return input switch
            {
                'A' => SelectedObject.Rock,
                'B' => SelectedObject.Paper,
                'C' => SelectedObject.Scissors,
                _ => throw new ArgumentOutOfRangeException(nameof(input))
            };
        }

        private SelectedObject GetSelfObject(char input)
        {
            return input switch
            {
                'X' => SelectedObject.Rock,
                'Y' => SelectedObject.Paper,
                'Z' => SelectedObject.Scissors,
                _ => throw new ArgumentOutOfRangeException(nameof(input))
            };
        }

        private int GetSelfObjectPartTwo(char input)
        {
            return input switch
            {
                'X' => 0,
                'Y' => 3,
                'Z' => 6,
                _ => throw new ArgumentOutOfRangeException(nameof(input))
            };
        }

        private int CalculateWin(SelectedObject elf, SelectedObject self)
        {
            if (self - elf == 0)
            {
                return 3;
            }
            else if (self - elf == 1 || self - elf == -2)
            {
                return 6;
            }
            else
            {
                return 0;
            }
        }

        private int CalculateWin(SelectedObject elf, int score)
        {
            if (score == 3)
            {
                return (int) elf;
            }
            else if (score == 0)
            {
                return elf == SelectedObject.Rock ? 3 : (int) elf - 1;
            }
            else
            {
                return ((int)elf % 3) + 1;
            }
        }
    }
}
