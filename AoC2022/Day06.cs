﻿using AoCHelper;

namespace AoC2022
{
    public partial class Day06 : BaseDay
    {
        private readonly string _input;

        public Day06()
        {
            _input = File.ReadAllText(InputFilePath);

        }

        public override ValueTask<string> Solve_1()
        {
            var signal = _input.Skip(3).Select((v, i) => new { value = v, index = i}).SkipWhile(v => AnyMatch(v.value, v.index, 4));
            return ValueTask.FromResult((signal.First().index + 4).ToString());
        }

        public override ValueTask<string> Solve_2()
        {
            var signal = _input.Skip(13).Select((v, i) => new { value = v, index = i }).SkipWhile(v => AnyMatch(v.value, v.index, 14));
            return ValueTask.FromResult((signal.First().index + 14).ToString());
        }

        private bool AnyMatch(char c, int i, int count)
        {
            var list = new List<char>() { c };
            for (int j = 0; j < count; j++)
            {
                list.Add(_input[i + j]);
            }
            return list.Distinct().Count() != count;
        }
    }
}
