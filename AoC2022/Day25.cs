using AoCHelper;
using System.Numerics;

namespace AoC2022
{
    public partial class Day25 : BaseDay
    {
        private readonly string _input;

        public Day25()
        {
            _input = File.ReadAllText(InputFilePath);
        }

        public override ValueTask<string> Solve_1()
        {
            var base10 = _input.Split(Environment.NewLine).Select(line => SNAFUToBase10(line));
            return ValueTask.FromResult(Base10ToSNAFU(base10.Sum()));
        }

        public override ValueTask<string> Solve_2()
        {
            return ValueTask.FromResult("");
        }

        private string Base10ToSNAFU(long num) 
        {
            string val = string.Empty;
            do
            {
                var next = (int) (num % 5);
                val =  next switch
                {
                    0 => '0',
                    1 => '1',
                    2 => '2',
                    3 => '=',
                    4 => '-'
                } + val;
                num /= 5;
                if (next > 2) num++;
            }
            while (num > 0);

            return val;
        }

        private long SNAFUToBase10(string num)
        {
            var val = 0L;
            var position = 0;
            foreach (var c in num.Reverse())
            {
                var five = (long)Math.Pow(5, position++);
                val += five * c switch
                {
                    '=' => -2,
                    '-' => -1,
                    '0' => 0,
                    '1' => 1,
                    '2' => 2
                };
            }
            return val;
        }
    }
}
