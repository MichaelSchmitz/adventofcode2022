﻿using AoCHelper;

namespace AoC2022
{
    public class Day04 : BaseDay
    {

        private readonly string _input;

        public Day04()
        {
            _input = File.ReadAllText(InputFilePath);
        }

        public override ValueTask<string> Solve_1()
        {
            var pairs = _input.Split(Environment.NewLine).Select(p => new Pair(p));
            pairs = pairs.Where(p => p.IsContainedInOneAnother());
            return ValueTask.FromResult(pairs.Count().ToString());
        }

        public override ValueTask<string> Solve_2()
        {
            var pairs = _input.Split(Environment.NewLine).Select(p => new Pair(p));
            pairs = pairs.Where(p => p.IsOverlapping());
            return ValueTask.FromResult(pairs.Count().ToString());
        }

        private class Pair
        {
            public (int Start, int End) FirstElf { get; private set; }
            public (int Start, int End) SecondElf { get; private set; }

            public Pair(string p)
            {
                var elves = p.Split(',');
                FirstElf = GetRange(elves[0]);
                SecondElf = GetRange(elves[1]);
            }

            public bool IsContainedInOneAnother()
            {
                return (FirstElf.Start <= SecondElf.Start && FirstElf.End >= SecondElf.End) ||
                    (SecondElf.Start <= FirstElf.Start && SecondElf.End >= FirstElf.End);
            }

            public bool IsOverlapping()
            {
                return !(SecondElf.Start > FirstElf.End || FirstElf.Start > SecondElf.End);
            }

            private (int, int) GetRange(string e)
            {
                var borders = e.Split('-').Select(_e => int.Parse(_e)).ToList();
                return (borders[0], borders[1]);
            }
        }
    }
}
