using AoCHelper;
using Spectre.Console;
using System.Collections.Immutable;

namespace AoC2022
{
    public partial class Day22 : BaseDay
    {
        public static bool SkipPartTwo = false;
        private readonly string _input;

        public Day22()
        {
            _input = File.ReadAllText(InputFilePath);
        }

        public override ValueTask<string> Solve_1()
        {
            (var tiles, var instructions) = ParseInput(_input);
            (var tile, var facing) = MoveAlongMap(tiles, instructions);
            return ValueTask.FromResult((tile.ComputeResult() + (int)facing).ToString());
        }
        public override ValueTask<string> Solve_2()
        {
            if (SkipPartTwo) return ValueTask.FromResult("");
            (var tiles, var instructions) = ParseInput(_input);
            (var tile, var facing) = MoveAlongCube(tiles, instructions);
            return ValueTask.FromResult((tile.ComputeResult() + (int)facing).ToString());
        }

        private (Tile tile, Direction facing) MoveAlongMap(IDictionary<(int x, int y), Tile> tiles, IEnumerable<Instruction> instructions)
        {
            var currentTile = tiles.Where(entry => entry.Key.y == 0).MinBy(entry => entry.Key.x).Value;
            var facing = Direction.Right;
            foreach (var instruction in instructions)
            {
                if (instruction is Forward fwd)
                {
                    switch (facing)
                    {
                        case Direction.Right:
                            for (int i = 0; i < fwd.Amount; i++)
                            {
                                if (!tiles.TryGetValue((currentTile.Key.x + 1, currentTile.Key.y), out var nextTile))
                                {
                                    nextTile = tiles.Where(entry => entry.Key.y == currentTile.Key.y).MinBy(entry => entry.Key.x).Value;
                                }
                                if (nextTile.IsWalkable)
                                {
                                    currentTile = nextTile;
                                }
                                else
                                {
                                    break;
                                }
                            }
                            break;
                        case Direction.Left:
                            for (int i = 0; i < fwd.Amount; i++)
                            {
                                if (!tiles.TryGetValue((currentTile.Key.x - 1, currentTile.Key.y), out var nextTile))
                                {
                                    nextTile = tiles.Where(entry => entry.Key.y == currentTile.Key.y).MaxBy(entry => entry.Key.x).Value;
                                }
                                if (nextTile.IsWalkable)
                                {
                                    currentTile = nextTile;
                                }
                                else
                                {
                                    break;
                                }
                            }
                            break;
                        case Direction.Down:
                            for (int i = 0; i < fwd.Amount; i++)
                            {
                                if (!tiles.TryGetValue((currentTile.Key.x, currentTile.Key.y + 1), out var nextTile))
                                {
                                    nextTile = tiles.Where(entry => entry.Key.x == currentTile.Key.x).MinBy(entry => entry.Key.y).Value;
                                }
                                if (nextTile.IsWalkable)
                                {
                                    currentTile = nextTile;
                                }
                                else
                                {
                                    break;
                                }
                            }
                            break;
                        case Direction.Up:
                            for (int i = 0; i < fwd.Amount; i++)
                            {
                                if (!tiles.TryGetValue((currentTile.Key.x, currentTile.Key.y - 1), out var nextTile))
                                {
                                    nextTile = tiles.Where(entry => entry.Key.x == currentTile.Key.x).MaxBy(entry => entry.Key.y).Value;
                                }
                                if (nextTile.IsWalkable)
                                {
                                    currentTile = nextTile;
                                }
                                else
                                {
                                    break;
                                }
                            }
                            break;
                    }
                }
                else if (instruction is Turn turn)
                {
                    facing = turn.GetNewDirection(facing);
                }
            }
            return (currentTile, facing);
        }

        private (Tile tile, Direction facing) MoveAlongCube(IDictionary<(int x, int y), Tile> tiles, IEnumerable<Instruction> instructions)
        {
            var currentTile = tiles.Where(entry => entry.Key.y == 0 && entry.Value.IsWalkable).MinBy(entry => entry.Key.x).Value;
            var facing = Direction.Right;
            foreach (var instruction in instructions)
            {
                if (instruction is Forward fwd)
                {
                    for (int i = 0; i < fwd.Amount; i++)
                    {
                        Direction? newDirection = null;
                        Tile nextTile;
                        switch (facing)
                        {
                            case Direction.Right:
                                if (!tiles.TryGetValue((currentTile.Key.x + 1, currentTile.Key.y), out nextTile))
                                {
                                    if (currentTile.Key.y < 50)
                                    {
                                        nextTile = tiles[(99, 149 - currentTile.Key.y)];
                                        newDirection = Direction.Left;
                                    }
                                    else if (currentTile.Key.y < 100)
                                    {
                                        nextTile = tiles[(currentTile.Key.y + 50, 49)];
                                        newDirection = Direction.Up;
                                    }
                                    else if (currentTile.Key.y < 150)
                                    {
                                        nextTile = tiles[(149, 149 - currentTile.Key.y)];
                                        newDirection = Direction.Left;
                                    }
                                    else
                                    {
                                        nextTile = tiles[(currentTile.Key.y - 100, 149)];
                                        newDirection = Direction.Up;
                                    }
                                }
                                if (nextTile.IsWalkable)
                                {
                                    currentTile = nextTile;
                                    facing = newDirection ?? facing;
                                }
                                else
                                {
                                    i = fwd.Amount;
                                }
                                break;
                            case Direction.Left:
                                if (!tiles.TryGetValue((currentTile.Key.x - 1, currentTile.Key.y), out nextTile))
                                {
                                    if (currentTile.Key.y < 50)
                                    {
                                        nextTile = tiles[(0, 149 - currentTile.Key.y)];
                                        newDirection = Direction.Right;
                                    }
                                    else if (currentTile.Key.y < 100)
                                    {
                                        nextTile = tiles[(currentTile.Key.y - 50, 100)];
                                        newDirection = Direction.Down;
                                    }
                                    else if (currentTile.Key.y < 150)
                                    {
                                        nextTile = tiles[(50, 149 - currentTile.Key.y)];
                                        newDirection = Direction.Right;
                                    }
                                    else
                                    {
                                        nextTile = tiles[(currentTile.Key.y - 100, 0)];
                                        newDirection = Direction.Down;
                                    }
                                }
                                if (nextTile.IsWalkable)
                                {
                                    currentTile = nextTile;
                                    facing = newDirection ?? facing;
                                }
                                else
                                {
                                    i = fwd.Amount;
                                }
                                break;
                            case Direction.Down:
                                if (!tiles.TryGetValue((currentTile.Key.x, currentTile.Key.y + 1), out nextTile))
                                {
                                    if (currentTile.Key.x < 50)
                                    {
                                        nextTile = tiles[(currentTile.Key.x + 100, 0)];
                                    }
                                    else if (currentTile.Key.x < 100)
                                    {
                                        nextTile = tiles[(49, currentTile.Key.x + 100)];
                                        newDirection = Direction.Left;
                                    }
                                    else
                                    {
                                        nextTile = tiles[(99, currentTile.Key.x - 50)];
                                        newDirection = Direction.Left;
                                    }
                                }
                                if (nextTile.IsWalkable)
                                {
                                    currentTile = nextTile;
                                    facing = newDirection ?? facing;
                                }
                                else
                                {
                                    i = fwd.Amount;
                                }
                                break;
                            case Direction.Up:
                                if (!tiles.TryGetValue((currentTile.Key.x, currentTile.Key.y - 1), out nextTile))
                                {
                                    if (currentTile.Key.x < 50)
                                    {
                                        nextTile = tiles[(50, currentTile.Key.x + 50)];
                                        newDirection = Direction.Right;
                                    }
                                    else if (currentTile.Key.x < 100)
                                    {
                                        nextTile = tiles[(0, currentTile.Key.x + 100)];
                                        newDirection = Direction.Right;
                                    }
                                    else
                                    {
                                        nextTile = tiles[(currentTile.Key.x - 100, 199)];
                                    }
                                }
                                if (nextTile.IsWalkable)
                                {
                                    currentTile = nextTile;
                                    facing = newDirection ?? facing;
                                }
                                else
                                {
                                    i = fwd.Amount;
                                }
                                break;
                        }
                    }
                }
                else if (instruction is Turn turn)
                {
                    facing = turn.GetNewDirection(facing);
                }
            }
            return (currentTile, facing);
        }


        private (IDictionary<(int x, int y), Tile> tiles, IEnumerable<Instruction> instructions) ParseInput(string input)
        {
            var split = input.Split($"{Environment.NewLine}{Environment.NewLine}");
            var tiles = split[0].Split(Environment.NewLine).SelectMany((line, y) => line.Select((c, x) => {
                if (c != ' ') return (coords: (x, y), tile: new Tile(c, (x, y)));
                else return ((0, 0), null);
                }))
                .Where(entry => entry.tile != null)
                .ToImmutableDictionary(entry => entry.coords, entry => entry.tile);
            var instructions = GetInstructions(split[1]);

            return (tiles, instructions);
        }

        private IEnumerable<Instruction> GetInstructions(string instructions)
        {
            var parsed = new List<Instruction>();
            var number = "";
            foreach (char c in instructions)
            {
                if (char.IsLetter(c))
                {
                    parsed.Add(new Forward(number));
                    parsed.Add(new Turn(c));
                    number = "";
                } 
                else
                {
                    number += c;
                }
            }
            if (number != "")
            {
                parsed.Add(new Forward(number));
            }
            return parsed;
        }

        private enum Direction
        {
            Right, Down, Left, Up
        }

        private interface Instruction { }

        private class Turn : Instruction
        {
            public Func<Direction, Direction> GetNewDirection { get; }
            public Turn(char c)
            {
                if (c == 'R')
                {
                    GetNewDirection = dir => (int) dir + 1 > 3 ? Direction.Right : dir + 1;
                }
                else
                {
                    GetNewDirection = dir => (int) dir - 1 < 0 ? Direction.Up : dir - 1;
                }
            }
        }

        private class Forward : Instruction
        {
            public int Amount { get; }

            public Forward(string number)
            {
                Amount = int.Parse(number);
            }
        }

        private class Tile
        {
            public (int x, int y) Key { get; }
            public bool IsWalkable { get; }
            public Tile(char c, (int x, int y) key)
            {
                IsWalkable = c == '.';
                Key = key;
            }

            public int ComputeResult()
            {
                return (Key.y + 1) * 1000 + (Key.x + 1) * 4;
            }
        }
    }
}
