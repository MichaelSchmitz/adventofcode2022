﻿using AoCHelper;

namespace AoC2022
{
    public partial class Day09 : BaseDay
    {
        private readonly string _input;

        public Day09()
        {
            _input = File.ReadAllText(InputFilePath);

        }

        public override ValueTask<string> Solve_1()
        {
            var steps = _input.Split(Environment.NewLine).Select(line => new Step(line));
            return ValueTask.FromResult(DoStepsForOneTail(steps).Count().ToString());
        }

        public override ValueTask<string> Solve_2()
        {
            var steps = _input.Split(Environment.NewLine).Select(line => new Step(line));
            return ValueTask.FromResult(DoStepsForMultipleTails(steps, 10).Count().ToString());
        }

        private HashSet<(int x, int y)> DoStepsForOneTail(IEnumerable<Step> steps)
        {
            var startPosition = (x: 0, y: 0);
            var headPosition = startPosition;
            var tailPosition = startPosition;
            var visited = new HashSet<(int x, int y)> { tailPosition };
            foreach (var step in steps)
            {
                for (int i = 0; i < step.Amount; ++i)
                {
                    headPosition = MoveBy(headPosition, step.Move);
                    if (!IsNear(headPosition, tailPosition))
                    {
                        tailPosition = MoveBy(headPosition, (step.Move.x * -1, step.Move.y * -1));
                        if (!visited.Contains(tailPosition))
                            visited.Add(tailPosition);
                    }
                }
            }
            return visited;
        }

        private HashSet<(int x, int y)> DoStepsForMultipleTails(IEnumerable<Step> steps, int tailCount)
        {
            var startPosition = (x: 0, y: 0);
            var tails = new List<(int x, int y)>();
            for (int i = 0; i < tailCount; i++)
            {
                tails.Add(startPosition);
            }
            (int x, int y) lastTail;
            (int x, int y) currentTail;
            var visited = new HashSet<(int x, int y)> { startPosition };
            foreach (var step in steps)
            {
                for (int i = 0; i < step.Amount; ++i)
                {
                    currentTail = MoveBy(tails[0], step.Move);
                    tails[0] = currentTail;
                    for (int j = 1; j < tailCount; ++j)
                    {
                        lastTail = currentTail;
                        currentTail = tails[j];
                        if (!IsNear(lastTail, currentTail))
                        {
                            if (lastTail.x != currentTail.x)
                            {
                                currentTail.x += lastTail.x > currentTail.x ? 1 : -1;
                            }
                            if (lastTail.y != currentTail.y)
                            {
                                currentTail.y += lastTail.y > currentTail.y ? 1: -1;
                            }
                            tails[j] = currentTail;
                        }
                    }
                    if (!visited.Contains(tails[tailCount - 1]))
                        visited.Add(tails[tailCount - 1]);
                }
            }
            return visited;
        }

        private (int x, int y) MoveBy((int x, int y) position, (int x, int y) move)
        {
            return (position.x + move.x, position.y + move.y);
        }


        private bool IsNear((int x, int y) head, (int x, int y) tail)
        {
            return Math.Abs(head.x - tail.x) < 2 && Math.Abs(head.y - tail.y) < 2;
        }

        private class Step
        {
            public int Amount { get; private set; }
            public (int x, int y) Move { get; private set; }

            public Step(string line)
            {
                var step = line.Split(' ');
                Amount = int.Parse(step[1]);
                Move = step[0] switch
                {
                    "R" => (1, 0),
                    "L" => (-1, 0),
                    "U" => (0, -1),
                    "D" => (0, 1)
                };
            }
        }
    }
}
