using AoCHelper;
using System.Collections.Immutable;

namespace AoC2022
{
    public partial class Day20 : BaseDay
    {
        private readonly string _input;

        public Day20()
        {
            _input = File.ReadAllText(InputFilePath);
        }

        public override ValueTask<string> Solve_1()
        {
            var encrypted = _input.Split(Environment.NewLine).Select((line, index) => (index, long.Parse(line))).ToImmutableArray();
            var unencrypted = MixFile(encrypted);
            return ValueTask.FromResult(CalculateGrooveCoordinates(unencrypted).Sum().ToString());
        }

        public override ValueTask<string> Solve_2()
        {
            var encrypted = _input.Split(Environment.NewLine).Select((line, index) => (index, long.Parse(line) * 811589153L)).ToImmutableArray();
            var unencrypted = MixFile(encrypted, 10);
            return ValueTask.FromResult(CalculateGrooveCoordinates(unencrypted).Sum().ToString());
        }        

        private List<(int index, long value)> MixFile(ImmutableArray<(int index, long value)> encrypted, int rounds = 1)
        {
            var unencrpyted = new List<(int index, long value)>(encrypted);
            var size = encrypted.Length - 1;
            for (int i = 0; i < rounds; ++i)
            {
                foreach (var val in encrypted)
                {
                    var index = unencrpyted.IndexOf(val);
                    var newIndex = (int)((index + val.value) % size);
                    if (newIndex < 0)
                        newIndex += size;

                    unencrpyted.RemoveAt(index);
                    unencrpyted.Insert(newIndex, val);

                }
            }
            return unencrpyted;
        }

        private List<long> CalculateGrooveCoordinates(List<(int index, long value)> unencrypted)
        {
            var baseIndex = unencrypted.FindIndex(e => e.value == 0);
            return new() { 
                unencrypted[(baseIndex + 1000) % unencrypted.Count].value, 
                unencrypted[(baseIndex + 2000) % unencrypted.Count].value,
                unencrypted[(baseIndex + 3000) % unencrypted.Count].value
            };
        }
    }
}
