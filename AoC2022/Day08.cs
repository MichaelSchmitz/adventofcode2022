﻿using AoCHelper;
using System.Collections.Immutable;

namespace AoC2022
{
    public partial class Day08 : BaseDay
    {
        private readonly string _input;

        public Day08()
        {
            _input = File.ReadAllText(InputFilePath);

        }

        public override ValueTask<string> Solve_1()
        {
            var gridSize = _input.IndexOf(Environment.NewLine);
            var trees = _input.Split(Environment.NewLine).SelectMany((l, y) => l.Select((c, x) => new Tree(x, y, c - '0'))).ToImmutableArray();
            return ValueTask.FromResult(FindVisible(trees, gridSize).ToString());
        }

        public override ValueTask<string> Solve_2()
        {
            var trees = _input.Split(Environment.NewLine).Select((l, y) => l.Select((c, x) => new Tree(x, y, c - '0')));
            return ValueTask.FromResult(FindScenicScores(trees).Max().ToString());
        }

        private List<int> FindScenicScores(IEnumerable<IEnumerable<Tree>> trees)
        {
            var maxX = trees.ElementAt(0).Count() - 1;
            var maxY = trees.Count() - 1; 
            var treesFlattend = trees.SelectMany(tree => tree);
            var treesNotAtBorder = treesFlattend.Where(tree => tree.Position.x != 0 && tree.Position.y != 0 && tree.Position.x != maxX && tree.Position.y != maxY);
            var treeMap = treesFlattend.ToDictionary(tree => tree.Position);
            return treesNotAtBorder.Select(tree => FindScenicScore(treeMap, tree, maxX, maxY)).ToList();
        }

        private int FindScenicScore(Dictionary<(int, int), Tree> trees, Tree tree, int maxX, int maxY)
        {
            var score = 1;

            var subScore = 0;
            for (int x = tree.Position.x + 1; x <= maxX; x++)
            {
                subScore++;
                if (trees[(x, tree.Position.y)].Height >= tree.Height)
                {
                    break;
                } 
            }
            score *= subScore;
            subScore = 0;
            for (int x = tree.Position.x - 1; x >= 0; x--)
            {
                subScore++;
                if (trees[(x, tree.Position.y)].Height >= tree.Height)
                {
                    break;
                }
            }
            score *= subScore;
            subScore = 0;
            for (int y = tree.Position.y + 1; y <= maxY; y++)
            {
                subScore++;
                if (trees[(tree.Position.x, y)].Height >= tree.Height)
                {
                    break;
                }
            }
            score *= subScore;
            subScore = 0;
            for (int y = tree.Position.y - 1; y >= 0; y--)
            {
                subScore++;
                if (trees[(tree.Position.x, y)].Height >= tree.Height)
                {
                    break;
                }
            }
            score *= subScore;
            return score;
        }

        private int FindVisible(IEnumerable<Tree> trees, int gridSize)
        {
            var count = 0;
            for(int y = 1; y < gridSize - 1; y++)  {
                for (int x = 1; x < gridSize - 1; x++)
                {
                    if (FromRight(trees, x, y, gridSize) || FromLeft(trees, x, y, gridSize) ||
                        FromTop(trees, x, y, gridSize) || FromBottom(trees, x, y, gridSize))
                    {
                        count++;
                    }
                }
            }
            return count + 4 * gridSize  - 4; // do not count edges twice
        }

        private bool FromRight(IEnumerable<Tree> trees, int x, int y, int gridSize)
        {
            var tree = trees.ElementAt(x + y * gridSize);
            for (int i = x + 1; i < gridSize; i++)
            {
                if (trees.ElementAt(i + y * gridSize).Height >= tree.Height)
                {
                    return false;
                }
            }
            return true;
        }

        private bool FromLeft(IEnumerable<Tree> trees, int x, int y, int gridSize)
        {
            var tree = trees.ElementAt(x + y * gridSize);
            for (int i = x - 1; i >= 0; i--)
            {
                if (trees.ElementAt(i + y * gridSize).Height >= tree.Height)
                {
                    return false;
                }
            }
            return true;
        }

        private bool FromTop(IEnumerable<Tree> trees, int x, int y, int gridSize)
        {
            var tree = trees.ElementAt(x + y * gridSize);
            for (int i = y - 1; i >= 0; i--)
            {
                if (trees.ElementAt(x + i * gridSize).Height >= tree.Height)
                {
                    return false;
                }
            }
            return true;
        }

        private bool FromBottom(IEnumerable<Tree> trees, int x, int y, int gridSize)
        {
            var tree = trees.ElementAt(x + y * gridSize);
            for (int i = y + 1; i < gridSize; i++)
            {
                if (trees.ElementAt(x + i * gridSize).Height >= tree.Height)
                {
                    return false;
                }
            }
            return true;
        }

        private class Tree
        {
            public (int x, int y) Position { get; private set; }
            public int Height { get; private set; }

            public Tree(int x, int y, int height)
            {
                Position = (x, y);
                Height = height;
            }
        }
    }
}
