﻿using AoCHelper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AoC2022
{
    public class Day03 : BaseDay
    {

        private readonly string _input;

        public Day03()
        {
            _input = File.ReadAllText(InputFilePath);
        }

        public override ValueTask<string> Solve_1()
        {
            var backpacks = _input.Split(Environment.NewLine).Select(b => new Backpack(b));
            return ValueTask.FromResult(backpacks.Select(b => b.GetDuplicateItem()).Sum().ToString());
        }

        public override ValueTask<string> Solve_2()
        {
            var backpacks = _input.Split(Environment.NewLine).Select(b => new Backpack(b));
            var index = 0;
            var badges = backpacks.GroupBy(b => index++ / 3).Select(g => GetBadge(g));

            return ValueTask.FromResult(badges.Sum().ToString());
        }

        private static int GetBadge(IGrouping<int, Backpack> backpacks)
        {

            return backpacks.First().All.Where(i => backpacks.ElementAt(1).All.Contains(i) && backpacks.ElementAt(2).All.Contains(i)).FirstOrDefault(int.MinValue);
        }

        private class Backpack
        {
            public List<int> CompartmentA { get; private set; }
            public List<int> CompartmentB { get; private set; }
            public IEnumerable<int> All { get => CompartmentA.Concat(CompartmentB); }


            public Backpack(string items)
            {
                CompartmentA = GetCompartmentItems(items[..(items.Length / 2)]);
                CompartmentB = GetCompartmentItems(items[(items.Length / 2)..]);
            }

            public int GetDuplicateItem()
            {
                return CompartmentA.Where(i => CompartmentB.Contains(i)).FirstOrDefault(int.MinValue);
            }

            public static int GetPriority(char c)
            {
                return Char.IsLower(c) ? (int)c - 96 : (int)c - 38;
            }

            private List<int> GetCompartmentItems(string items)
            {
                var parsed = new List<int>();
                foreach (char c in items)
                {
                    parsed.Add(GetPriority(c));
                }
                return parsed;
            }
        }
    }
}
