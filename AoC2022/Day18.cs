using AoCHelper;
using System.Collections.Immutable;
using System.Linq;

namespace AoC2022
{
    public partial class Day18 : BaseDay
    {
        private readonly string _input;

        public Day18()
        {
            _input = File.ReadAllText(InputFilePath);
        }

        public override ValueTask<string> Solve_1()
        {
            var cubes = _input.Split(Environment.NewLine).Select(line => new Cube(line)).ToImmutableArray();
            return ValueTask.FromResult(cubes.Select(cube => cube.ExposedSides(cubes)).Sum().ToString());
        }

        public override ValueTask<string> Solve_2()
        {
            var cubes = _input.Split(Environment.NewLine).Select(line => new Cube(line)).ToImmutableArray();
            var water = FillWithWater(cubes);
            return ValueTask.FromResult(cubes.SelectMany(GetNeighbours).Count(cube => water.Contains(cube)).ToString());
        }

        private IEnumerable<Cube> FillWithWater(ImmutableArray<Cube> cubes)
        {
            var waterCovered = new HashSet<Cube>();
            var current = new Queue<Cube>();
            var min = new Cube(cubes.MinBy(cube => cube.X).X - 1,
                cubes.MinBy(cube => cube.Y).Y - 1,
                cubes.MinBy(cube => cube.Z).Z - 1);
            var max = new Cube(cubes.MaxBy(cube => cube.X).X + 1,
                cubes.MaxBy(cube => cube.Y).Y + 1,
                cubes.MaxBy(cube => cube.Z).Z + 1);

            waterCovered.Add(min);
            current.Enqueue(min);
            while (current.Any())
            {
                var next = current.Dequeue();
                foreach (var neighbour in GetNeighbours(next))
                {
                    if (!waterCovered.Contains(neighbour) && InRange(min, max, neighbour) && !cubes.Contains(neighbour))
                    {
                        waterCovered.Add(neighbour);
                        current.Enqueue(neighbour);
                    }
                }
            }
            return waterCovered;
        }

        private static bool InRange(Cube min, Cube max, Cube cube)
        {
            return min.X <= cube.X && cube.X <= max.X &&
                min.Y <= cube.Y && cube.Y <= max.Y &&
                min.Z <= cube.Z && cube.Z <= max.Z;
        }

        private List<Cube> GetNeighbours(Cube cube) => new()
        {
            new(cube.X, cube.Y, cube.Z + 1),
            new(cube.X, cube.Y, cube.Z - 1),
            new(cube.X, cube.Y + 1, cube.Z),
            new(cube.X, cube.Y - 1, cube.Z),
            new(cube.X + 1, cube.Y, cube.Z),
            new(cube.X - 1, cube.Y, cube.Z)
        };

        // use record for easy equality / contain checks
        private record class Cube
        {
            public int X { get; }
            public int Y { get; }
            public int Z { get; }

            public Cube(int x, int y, int z)
            {
                X = x;
                Y = y;
                Z = z;
            }

            public Cube(string line)
            {
                var split = line.Split(',');
                X = int.Parse(split[0]);
                Y = int.Parse(split[1]);
                Z = int.Parse(split[2]);
            }

            public int ExposedSides(IEnumerable<Cube> cubes)
            {
                var xCovered = cubes.Where(cube => cube.Y == Y && cube.Z == Z && Math.Abs(X - cube.X) == 1).Count();
                var yCovered = cubes.Where(cube => cube.X == X && cube.Z == Z && Math.Abs(Y - cube.Y) == 1).Count();
                var zCovered = cubes.Where(cube => cube.X == X && cube.Y == Y && Math.Abs(Z - cube.Z) == 1).Count();
                return 6 - xCovered - yCovered - zCovered;
            }
        }
    }
}
