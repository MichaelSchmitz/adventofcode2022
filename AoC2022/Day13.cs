using AoCHelper;
using System.Text.Json;

namespace AoC2022
{
    public partial class Day13 : BaseDay
    {
        private readonly string _input;

        public Day13()
        {
            _input = File.ReadAllText(InputFilePath);
        }

        public override ValueTask<string> Solve_1()
        {
            var packetPairs = _input.Split($"{Environment.NewLine}{Environment.NewLine}").Select(pair => pair.Split(Environment.NewLine).Select(entry => JsonSerializer.Deserialize<JsonElement>(entry)));
            return ValueTask.FromResult(FindOrderedPairs(packetPairs).ToString());
        }

        public override ValueTask<string> Solve_2()
        {
            var withDividers = _input.Split(Environment.NewLine, StringSplitOptions.RemoveEmptyEntries).ToList();
            withDividers.Add("[[2]]");
            withDividers.Add("[[6]]");
            withDividers.Sort(Compare);
            var dividerOne = withDividers.IndexOf("[[2]]") + 1;
            var dividerTwo = withDividers.IndexOf("[[6]]") + 1;
            
            return ValueTask.FromResult((dividerOne * dividerTwo).ToString());
        }

        private int Compare(string a, string b)
        {
            var decision = MakeDecision(JsonSerializer.Deserialize<JsonElement>(a), JsonSerializer.Deserialize<JsonElement>(b));
            return decision switch
            {
                // reverse the list while sorting
                Decision.Correct => -1,
                Decision.Incorrect => 1,
                _ => 0
            };
        }

        private int FindOrderedPairs(IEnumerable<IEnumerable<JsonElement>> packetPairs)
        {
            var ordered = 0;
            for (int i = 0; i < packetPairs.Count(); i++)
            {
                var left = packetPairs.ElementAt(i).ElementAt(0);
                var right = packetPairs.ElementAt(i).ElementAt(1);
                var decision = MakeDecision(left, right);
                if (decision == Decision.Correct)
                {
                    ordered += i + 1;
                }
            }
            return ordered;
        }

        private Decision MakeDecision(JsonElement left, JsonElement right)
        {
            if (left.ValueKind == JsonValueKind.Number && right.ValueKind == JsonValueKind.Number)
            {
                var leftParsed = left.Deserialize<int>();
                var rightParsed = right.Deserialize<int>();
                return leftParsed < rightParsed ? Decision.Correct : leftParsed > rightParsed ? Decision.Incorrect : Decision.None;
            } 
            else
            {
                List<JsonElement> leftParsed = GetList(left);
                List<JsonElement> rightParsed = GetList(right);

                int i;
                for (i = 0; i < leftParsed.Count; i++)
                {
                    if (rightParsed.Count == i)
                    {
                        return Decision.Incorrect;
                    }
                    else
                    {
                        var decision = MakeDecision(leftParsed[i], rightParsed[i]);
                        if (decision != Decision.None)
                        {
                            return decision;
                        }
                    }
                }
                if (i < rightParsed.Count)
                {
                    return Decision.Correct;
                }
            }
            return Decision.None;
        }

        private List<JsonElement> GetList(JsonElement input)
        {
            if (input.ValueKind == JsonValueKind.Array)
            {
                return input.Deserialize<List<JsonElement>>();
            }
            else
            {
                return new List<JsonElement> { input };
            }
        }

        private enum Decision
        {
            None, Correct, Incorrect
        }
    }
}
