﻿using AoCHelper;
using System.Text.RegularExpressions;

namespace AoC2022
{
    public partial class Day05 : BaseDay
    {
        private readonly string _inputInstuctions;
        private readonly string _inputStartConfig;

        public Day05()
        {
            var _input = File.ReadAllText(InputFilePath).Split($"{Environment.NewLine}{Environment.NewLine}");
            _inputStartConfig = _input[0];
            _inputInstuctions = _input[1];

        }

        public override ValueTask<string> Solve_1()
        {
            var config = GetConfig(_inputStartConfig);
            foreach (var instruction in GetInstructionsForCrateMover9000(_inputInstuctions))
            {
                config = instruction.Invoke(config);
            }
            return ValueTask.FromResult(new string(config.Select(c => c.Pop()).ToArray()));
        }

        public override ValueTask<string> Solve_2()
        {
            var config = GetConfig(_inputStartConfig);
            foreach (var instruction in GetInstructionsForCrateMover9001(_inputInstuctions))
            {
                config = instruction.Invoke(config);
            }
            return ValueTask.FromResult(new string(config.Select(c => c.Pop()).ToArray()));
        }

        [GeneratedRegex(@".*?(\d+).*(\d).*(\d)")]
        private static partial Regex InstructionRegex();

        [GeneratedRegex(@"[\[ ][A-Z ][\] ] {0,1}")]
        private static partial Regex StapleRegex();

        private List<Stack<char>> GetConfig(string input)
        {
            var lines = input.Split(Environment.NewLine);
            var stapleRegex = StapleRegex();
            var stapleLines = lines.Select(l => stapleRegex.Matches(l)).Reverse();
            List<Stack<char>> stacks = new();
            foreach (var stapleLine in stapleLines)
            {
                for (int i = 0; i < stapleLine.Count; i++)
                {
                    if (stacks.Count == i)
                    {
                        stacks.Add(new());
                    }
                    if (stapleLine[i].Value.Trim() == "")
                    {
                        continue;
                    }
                    else
                    {
                        stacks[i].Push(stapleLine[i].Value.Trim()[1]); //[X] -> 1
                    }
                }
            }
            return stacks;
        }

        private List<Func<List<Stack<char>>, List<Stack<char>>>> GetInstructionsForCrateMover9000(string input)
        {
            var lines = input.Split(Environment.NewLine);
            List<Func<List<Stack<char>>, List<Stack<char>>>> instructions = new();
            var instructionRegex = InstructionRegex();
            foreach (var line in lines)
            {
                var match = instructionRegex.Match(line);
                _ = int.TryParse(match.Groups[1].Value, out var count);
                _ = int.TryParse(match.Groups[2].Value, out var from);
                _ = int.TryParse(match.Groups[3].Value, out var to);
                instructions.Add((input) =>
                {
                    for (int i = 0; i < count; i++)
                    {
                        input[to - 1].Push(input[from - 1].Pop());
                    }
                    return input;
                });
            }
            return instructions;
        }

        private List<Func<List<Stack<char>>, List<Stack<char>>>> GetInstructionsForCrateMover9001(string input)
        {
            var lines = input.Split(Environment.NewLine);
            List<Func<List<Stack<char>>, List<Stack<char>>>> instructions = new();
            var instructionRegex = InstructionRegex();
            foreach (var line in lines)
            {
                var match = instructionRegex.Match(line);
                _ = int.TryParse(match.Groups[1].Value, out var count);
                _ = int.TryParse(match.Groups[2].Value, out var from);
                _ = int.TryParse(match.Groups[3].Value, out var to);
                instructions.Add((input) =>
                {
                    var tmpStack = new Stack<char>();
                    for (int i = 0; i < count; i++)
                    {
                        tmpStack.Push(input[from - 1].Pop());
                    }
                    for (int i = 0; i < count; i++)
                    {
                        input[to - 1].Push(tmpStack.Pop());
                    }
                    return input;
                });
            }
            return instructions;
        }
    }
}
