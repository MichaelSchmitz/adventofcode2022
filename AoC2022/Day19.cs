using AoCHelper;

namespace AoC2022
{
    public partial class Day19 : BaseDay
    {
        private readonly string _input;
        public static bool SkipPartTwo = false;

        public Day19()
        {
            _input = File.ReadAllText(InputFilePath);
        }

        public override ValueTask<string> Solve_1()
        {
            var blueprints = _input.Split(Environment.NewLine).Select(line => new Blueprint(line));
            return ValueTask.FromResult(blueprints.Select(blueprint => (blueprint.ID, Geode: Simulate(blueprint, 24)))
                .Select(result => result.ID * result.Geode).Sum().ToString());
        }

        public override ValueTask<string> Solve_2()
        {
            if (SkipPartTwo)
                return ValueTask.FromResult("");
            var blueprints = _input.Split(Environment.NewLine).Select(line => new Blueprint(line)).Take(3);
            return ValueTask.FromResult(blueprints.Select(blueprint => Simulate(blueprint, 32))
                .Aggregate((a, b) => a*b).ToString());            
        }


        private int Simulate(Blueprint blueprint, int minutes)
        {
            return SimulateStep(blueprint, 0, 1, 0, 0, 0, 0, 0, 0, 0, minutes);
        }

        int SimulateStep(Blueprint blueprint, int ore, int oreRobots, int clay, int clayRobots,
                int obsidian, int obsidianRobots, int geode, int geodeRobots, int currentMinute, int totalMinutes)
        {
            bool buildOreRobotPreviously = false;
            bool buildClayRobotPreviously = false;
            bool buildObsidianRobotPreviously = false;
            bool buildGeodeRobotPreviously = false;

            int geodeCount = geode;

            for (int i = currentMinute; i < totalMinutes; i++)
            {
                var canBuildOreRobot = blueprint.CanBuildOreRobot(ore);
                var canBuildClayRobot = blueprint.CanBuildClayRobot(ore);
                var canBuildObsidianRobot = blueprint.CanBuildObsidianRobot(ore, clay); 
                var canBuildGeodeRobot = blueprint.CanBuildGeodeRobot(ore, obsidian);
                ore += oreRobots;
                clay += clayRobots;
                obsidian += obsidianRobots;
                geode += geodeRobots;

                if (canBuildGeodeRobot && !buildGeodeRobotPreviously)
                {
                    var newGeodeCount = SimulateStep(blueprint, ore - blueprint.GeodeRobot.Ore, oreRobots, clay, clayRobots, obsidian - blueprint.GeodeRobot.Obsidian, obsidianRobots, geode, geodeRobots + 1, i + 1, totalMinutes);
                    geodeCount = Math.Max(geodeCount, newGeodeCount);
                    buildGeodeRobotPreviously = true;
                }
                if (canBuildObsidianRobot && !buildObsidianRobotPreviously && obsidianRobots < blueprint.MaxObsidian)
                {
                    var newGeodeCount = SimulateStep(blueprint, ore - blueprint.ObsidianRobot.Ore, oreRobots, clay - blueprint.ObsidianRobot.Clay, clayRobots, obsidian, obsidianRobots + 1, geode, geodeRobots, i + 1, totalMinutes);
                    geodeCount = Math.Max(geodeCount, newGeodeCount);
                    buildObsidianRobotPreviously = true;
                }
                if (canBuildClayRobot && !buildClayRobotPreviously && clayRobots < blueprint.MaxClay)
                {
                    var newGeodeCount = SimulateStep(blueprint, ore - blueprint.ClayRobot, oreRobots, clay, clayRobots + 1, obsidian, obsidianRobots, geode, geodeRobots, i + 1, totalMinutes);
                    geodeCount = Math.Max(geodeCount, newGeodeCount);
                    buildClayRobotPreviously = true;
                }
                if (canBuildOreRobot && !buildOreRobotPreviously && oreRobots < blueprint.MaxOre)
                {
                    var newGeodeCount = SimulateStep(blueprint, ore - blueprint.OreRobot, oreRobots + 1, clay, clayRobots, obsidian, obsidianRobots, geode, geodeRobots, i + 1, totalMinutes);
                    geodeCount = Math.Max(geodeCount, newGeodeCount);
                    buildOreRobotPreviously = true;
                }

                geodeCount = Math.Max(geodeCount, geode);
            }
            return geodeCount;
        }

        private class Blueprint
        {
            public int ID { get; }
            public int OreRobot { get; }
            public int ClayRobot { get; }
            public (int Ore, int Clay) ObsidianRobot { get; }
            public (int Ore, int Obsidian) GeodeRobot { get; }
            public int MaxOre { get; }
            public int MaxClay { get; }
            public int MaxObsidian { get; }

            public Blueprint(string line)
            {
                var split = line.Split(':');
                ID = int.Parse(split[0].Split(' ')[1]);
                split = split[1].Split('.');
                OreRobot = int.Parse(split[0].Split(' ', StringSplitOptions.RemoveEmptyEntries)[4]);
                ClayRobot = int.Parse(split[1].Split(' ', StringSplitOptions.RemoveEmptyEntries)[4]);
                var obsidian = split[2].Split(' ', StringSplitOptions.RemoveEmptyEntries);
                ObsidianRobot = (int.Parse(obsidian[4]), int.Parse(obsidian[7]));
                var geode = split[3].Split(' ', StringSplitOptions.RemoveEmptyEntries);
                GeodeRobot = (int.Parse(geode[4]), int.Parse(geode[7]));
                MaxObsidian = GeodeRobot.Obsidian;
                MaxClay = ObsidianRobot.Clay;
                MaxOre = new int[] { OreRobot, ClayRobot, ObsidianRobot.Ore, GeodeRobot.Ore }.Max();
            }

            public bool CanBuildOreRobot(int ore) => OreRobot <= ore;
            public bool CanBuildClayRobot(int ore) => ClayRobot <= ore;
            public bool CanBuildObsidianRobot(int ore, int clay) => ObsidianRobot.Ore <= ore && ObsidianRobot.Clay <= clay;
            public bool CanBuildGeodeRobot(int ore, int obsidian) => GeodeRobot.Ore <= ore && GeodeRobot.Obsidian <= obsidian;
        }
    }
}
