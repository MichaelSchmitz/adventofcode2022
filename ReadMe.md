# [Advent of Code 2022](https://adventofcode.com/2022)
![C#](https://img.shields.io/badge/Language-C%23-blue?style=flat)
![License MIT](https://img.shields.io/gitlab/license/MichaelSchmitz/adventofcode2022?style=flat)

Puzzle input is to be provided in the subfolder [Aoc2022/Inputs](Aoc2022/Inputs) in the form "XX.txt" where XX is the zero-padded day number.

To create a new day simply execute the given script `genNewDay.sh <DayNumber>`.
