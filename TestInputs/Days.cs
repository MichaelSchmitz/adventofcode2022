using NUnit.Framework;
using AoCHelper;
using AoC2022;

namespace TestInputs
{
    public class Days
    {
        [OneTimeSetUp]
        public void OneTimeSetUp()
        {
            Day15.Part1_Row = 10;
            Day15.Part2_Space = 20;
            Day19.SkipPartTwo = true;
            Day22.SkipPartTwo = true;
        }

        [TestCase(typeof(Day01), "24000", "45000")]
        [TestCase(typeof(Day02), "15", "12")]
        [TestCase(typeof(Day03), "157", "70")]
        [TestCase(typeof(Day04), "2", "4")]
        [TestCase(typeof(Day05), "CMZ", "MCD")]
        [TestCase(typeof(Day06), "7", "19")]
        [TestCase(typeof(Day07), "95437", "24933642")]
        [TestCase(typeof(Day08), "21", "8")]
        [TestCase(typeof(Day09), "13", "1")]
        [TestCase(typeof(Day10), "13140", "##..##..##..##..##..##..##..##..##..##..\r\n###...###...###...###...###...###...###.\r\n####....####....####....####....####....\r\n#####.....#####.....#####.....#####.....\r\n######......######......######......####\r\n#######.......#######.......#######.....")]
        [TestCase(typeof(Day11), "10605", "2713310158")]
        [TestCase(typeof(Day12), "31", "29")]
        [TestCase(typeof(Day13), "13", "140")]
        [TestCase(typeof(Day14), "24", "93")]
        [TestCase(typeof(Day15), "26", "56000011")]
        [TestCase(typeof(Day16), "1651", "1707")]
        [TestCase(typeof(Day17), "3068", "1514285714288")]
        [TestCase(typeof(Day18), "64", "58")]
        [TestCase(typeof(Day19), "33", "")]
        [TestCase(typeof(Day20), "3", "1623178306")]
        [TestCase(typeof(Day21), "152", "301")]
        [TestCase(typeof(Day22), "6032", "")]
        [TestCase(typeof(Day23), "110", "20")]
        [TestCase(typeof(Day24), "18", "54")]
        [TestCase(typeof(Day25), "2=-1=0", "")]
        public async Task SolveDay(Type type, string expectedPartOne, string expectedPartTwo)
        {
            // Arrange
            var instance = Activator.CreateInstance(type) as BaseDay;

            // Act
            var partOne = await instance.Solve_1();
            var partTwo = await instance.Solve_2();

            // Assert
            Assert.Multiple(() =>
            {
                Assert.That(partOne, Is.EqualTo(expectedPartOne));
                Assert.That(partTwo, Is.EqualTo(expectedPartTwo));
            });
        }
    }
}